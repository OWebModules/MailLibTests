﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLibTests
{
    [TestClass]
    public class SmtpTests
    {
        [TestMethod]
        public async Task SmtpLoginLogoutTest()
        {
            var smtpConnector = new MailLib.SMTPConnector();

            var loginResult = await smtpConnector.Login(GlobalConfig.SmtpLoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var logoutResult = await smtpConnector.Logout();

            if (!logoutResult.IsOk)
            {
                Assert.Fail(logoutResult.Message);
            }
        }

        [TestMethod]
        public async Task SendMailTest()
        {
            var smtpConnector = new MailLib.SMTPConnector();

            var loginResult = await smtpConnector.Login(GlobalConfig.SmtpLoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var sendResult = await smtpConnector.SendMessage(GlobalConfig.SmtpSmpleMessage);

            if (!sendResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }
        }

        [TestMethod]
        public async Task SendMailTestWithAttachment()
        {
            var smtpConnector = new MailLib.SMTPConnector();

            var loginResult = await smtpConnector.Login(GlobalConfig.SmtpLoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var sendResult = await smtpConnector.SendMessage(GlobalConfig.SmtpMessageWithAttachment);

            if (!sendResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLibTests
{
    [TestClass]
    public class Pop3Tests
    {
        [TestMethod]
        public async Task Pop3LoginLogoutTest()
        {
            var pop3Connector = new MailLib.POP3Connector();

            var loginResult = await pop3Connector.Login(GlobalConfig.Pop3LoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var logoutResult = await pop3Connector.Logout();

            if (!logoutResult.IsOk)
            {
                Assert.Fail(logoutResult.Message);
            }
        }

        [TestMethod]
        public async Task Pop3GetMails()
        {
            var pop3Connector = new MailLib.POP3Connector();

            var loginResult = await pop3Connector.Login(GlobalConfig.Pop3LoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var getMailsResult = await pop3Connector.GetMails();
            

            if (!getMailsResult.IsOk)
            {
                Assert.Fail(getMailsResult.Message);
            }
        }

        [TestMethod]
        public async Task Pop3DeleteMails()
        {
            var pop3Connector = new MailLib.POP3Connector();

            var loginResult = await pop3Connector.Login(GlobalConfig.Pop3LoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var deleteAllMailsResult = await pop3Connector.DeleteAllMails();
            await pop3Connector.Logout();
            if (!deleteAllMailsResult.IsOk)
            {
                Assert.Fail(deleteAllMailsResult.Message);
            }
        }


    }
}

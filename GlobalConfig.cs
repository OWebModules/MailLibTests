﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLibTests
{
    public static class GlobalConfig
    {
        public static MailLib.Models.ImapLoginRequest ImapLoginRequest => new MailLib.Models.ImapLoginRequest
        {
            Server = "imap.gmx.net",
            Port = 993,
            Username = "tassilok@gmx.de",
            Password = "Luder23!Wilbo!9",
            UseSSL = true,
            ValidateServerCertificate = true,
            SecureSocketOptions = MailKit.Security.SecureSocketOptions.SslOnConnect
        };


        public static MailLib.Models.Pop3LoginRequest Pop3LoginRequest => new MailLib.Models.Pop3LoginRequest
        {
            Server = "pop.gmx.net",
            Port = 995,
            Username = "tassilok@gmx.de",
            Password = "Luder23!Wilbo!9",
            SecureSocketOptions = MailKit.Security.SecureSocketOptions.SslOnConnect
        };

        public static MailLib.Models.SmtpLoginRequest SmtpLoginRequest => new MailLib.Models.SmtpLoginRequest
        {
            Server = "mail.gmx.net",
            Port = 587,
            SecureSocketOptions = MailKit.Security.SecureSocketOptions.StartTls,
            Username = "tassilok@gmx.de",
            Password = "Luder23!Wilbo!9"
        };

        public static MailboxAddress From
        {
            get
            {
                var address = new MailboxAddress("Tassilo Koller", "tassilok@gmx.de");
                return address;
            }
        }

        public static MailboxAddress To
        {
            get
            {
                var address = new MailboxAddress("Tassilo Koller", "tassilok@gmx.de");
                return address;
            }
        }

        public static MimeEntity MailBody
        {
            get
            {
                var mailBodyBuilder = new BodyBuilder();
                mailBodyBuilder.HtmlBody = "Testbody";
                return mailBodyBuilder.ToMessageBody();
            }
        }

        public static MimeMessage SmtpSmpleMessage
        {
            get
            {
                

                var message = new MimeMessage();
                message.From.Add(From);
                message.To.Add(To);
                message.Subject = "Testsubject";
                message.Body = MailBody;
                return message;
            }
        }

        public static MimeMessage SmtpMessageWithAttachment
        {
            get
            {
                var message = new MimeMessage();
                message.From.Add(From);
                message.To.Add(To);
                message.Subject = "Testsubject With Attachment";
                var path = @".\Ressources\20180623_091527.jpg";
                var attachment = new MimePart("image", "gif")
                {
                    Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(path)
                };

                var multiPart = new Multipart("mixed");
                multiPart.Add(MailBody);
                multiPart.Add(attachment);

                message.Body = multiPart;

                return message;
            }
        }
    }
}

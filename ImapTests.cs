﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace MailLibTests
{
    [TestClass]
    public class ImapTests
    {
        [TestMethod]
        public async Task ImapLoginLogoutTest()
        {
            var imapConnector = new MailLib.IMAPConnector();

            var loginResult = await imapConnector.Login(GlobalConfig.ImapLoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var logoutResult = await imapConnector.Logout();

            if (!logoutResult.IsOk)
            {
                Assert.Fail(logoutResult.Message);
            }
        }

        [TestMethod]
        public async Task ImapGetMails()
        {
            var imapConnector = new MailLib.IMAPConnector();

            var loginResult = await imapConnector.Login(GlobalConfig.ImapLoginRequest);

            if (!loginResult.IsOk)
            {
                Assert.Fail(loginResult.Message);
            }

            var getMailsResult = await imapConnector.GetMailsOfInbox(new MailLib.Models.ImapGetMailsRequest
            {
                DeleteMails = true
            });

            if (!getMailsResult.IsOk)
            {
                Assert.Fail(getMailsResult.Message);
            }
        }


    }
}
